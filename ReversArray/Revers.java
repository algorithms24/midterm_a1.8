package ReversArray ;

import java.util.Scanner;

public class Revers {
    public static int[] reveraA(int A[]) {
        // revers
        for (int i = 0; i < A.length / 2; i++) { //loop แค่ครึ่งเดียว
            int t = A[i]; //ให้ t = A[i]
            A[i] = A[A.length - i - 1]; // A[i] = ตัวสุดท้ายของA[]
            A[A.length - i - 1] = t; // ตัวสุดท้ายของA[] = t
        }
        // Print revers
        for (int i = 0; i < A.length; i++) {
            System.out.print(A[i] + " ");
        }
        return A;
    }
static Scanner kb = new Scanner(System.in);
    public static void main(String[] args) {
        int n = kb.nextInt(); //input size array
        int A[] = new int[n] ; 
        for(int i = 0 ; i < n ; i++){ //input A[]
            A[i] = kb.nextInt();
        } 
        reveraA(A);
    }
}