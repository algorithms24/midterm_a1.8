package MaxSubSlow;

import java.util.Scanner;

public class MaxSubSlow{
public static int MaxSub(int[] A){
   int m = 0 ;
   for(int j = 1 ; j < A.length ; j++){
    for(int k = j ; k < A.length ; k++ ){
        int s= 0;
        for(int i = j ; i <= k ; i++){
            s= s+A[i];
            if(s>m){
                m=s;
            }
        }
    }
   }

   return m;
}
static Scanner kb = new Scanner(System.in);
 public static void main(String[] args) {
    int n = kb.nextInt();
    int A[] = new int[n] ;
    for(int i = 0 ; i < n ; i++){
        A[i] = kb.nextInt();
    } 
        int m = MaxSub(A);
        System.out.println(m);
    }

}